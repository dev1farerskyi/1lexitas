<?php
/**
 * 404 Page template.
 *
 * @package lex
 * @since 1.0.0
 *
 */

$not_found_text = get_field('not_found_text', 'option');

get_header(); ?>

    <div class="lex-secondary-page-header padding_xx">
        <div class="lex-secondary-page-header__background">
            <div class="lex-secondary-page-header__background-first">
                <img src="<?php echo V_TEMP_URL . '/assets/img/circle-first-2.svg'; ?>" alt=""/>
            </div>
            <div class="lex-secondary-page-header__background-second">
                <img src="<?php echo V_TEMP_URL . '/assets/img/circle-secondary-1.svg'; ?>" alt=""/>
            </div>
        </div>
        <div class="container">
            <div class="lex-secondary-page-header__content">
                <h2 class="lex-secondary-page-header__title mb-20"><?php esc_html_e('Oh oh, can’t see what you’re looking for?', V_PREFIX); ?></h2>
                <p class="lex-secondary-page-header__description mb-40"><?php esc_html_e('Looks like we can’t see it either. Enjoy some ophthalmology jokes instead.', V_PREFIX); ?></p>
                <div class="lex-secondary-page-header__btn">
                    <a class="lex-btn lex-btn_icon lex-btn_icon-blue lex-btn_secondary" href="<?php echo home_url(); ?>">
                        <?php esc_html_e('Return to Home', V_PREFIX); ?>
                        <?php get_template_part('template-parts/elements/secondary-btn-circle'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php if ( ! empty( $not_found_text ) ): ?>
        <div class="lex-404">
            <div class="container">
                <div class="lex-404__wrap">
                    <?php echo $not_found_text; ?>
                </div>
            </div>
        </div>
    <?php endif ?>


<?php get_footer(); ?>
