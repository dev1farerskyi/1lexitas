<?php
/*
 * Block Name: Join The Team Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$image = get_field('image');
$title = get_field('title');
$description = get_field('description');
$button = get_field('button');

$block_name = 'lex-join-the-team';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="lex-join-the-team__wrap">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <?php if ( ! empty( $image ) ): ?>
                        <div class="lex-join-the-team__image">
                            <img src="<?php echo esc_url($image['url']); ?>" alt="">
                        </div>
                    <?php endif ?>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <div class="lex-join-the-team__content">
                        <div class="lex-join-the-team__content-wrap" data-aos="fade-up" data-aos-duration="1000">
                            <?php if ( ! empty( $title ) ) : ?>
                                <h3 class="lex-join-the-team__title">
                                    <?php echo $title; ?>
                                </h3>
                            <?php endif ?>
                            <?php if ( ! empty( $description ) ) : ?>
                            <p class="lex-join-the-team__description mb-40">
                                <?php echo $description; ?>
                            </p>
                            <?php endif ?>
                            <div class="lex-join-the-team__btn">
                                <?php if ( ! empty( $button ) ) :
                                    $link_target = $button['target'] ? $button['target'] : '_self';  ?>
                                    <a class="lex-btn lex-btn_icon lex-btn_primary" href="<?php echo $button['url']; ?>" target="<?php echo $link_target; ?>">
                                        <?php echo $button['title']; ?>
                                        <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>