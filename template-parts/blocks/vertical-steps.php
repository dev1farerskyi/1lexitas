<?php
/*
 * Block Name: Vertical steps Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$image_or_video = get_field('image_or_video');
$title = get_field('title');
$repeater = get_field('repeater');
$primary_button = get_field('primary_button');

$block_name = 'lex-vertical-steps';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'media-style-1';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if (!empty($title)): ?>
            <h1 class="lex-vertical-steps__main-title"><?php echo $title; ?></h1>
        <?php endif ?>

        <div class="row">
            <div class="col-lg-6">
                <div class="lex-vertical-steps__media">
                    <div class="lex-vertical-steps__media-before"
                         data-aos="fade-up"
                         data-aos-offset="300"
                         data-aos-duration="700"></div>
                    <?php if (!empty($image_or_video)): ?>
                        <img class="lex-vertical-steps__image" src="<?php echo esc_url($image_or_video['url']); ?>"
                             alt=""/>
                    <?php endif ?>
                    <div class="lex-vertical-steps__media-after"
                         data-aos="fade-up"
                         data-aos-offset="100"
                         data-aos-duration="700"></div>
                </div>
            </div>

            <div class="col-lg-6">
                <?php if (!empty($repeater)) : ?>
                    <div class="lex-vertical-steps__list lex-vertical-steps__list-desktop">
                        <?php foreach ($repeater as $key => $row): ?>
                            <div class="lex-vertical-steps__list-item vertical-step-animated">
                                <?php if (!empty($row['number'])): ?>
                                    <div class="number"><span><?php echo $row['number']; ?></span></div>
                                <?php endif ?>

                                <?php if (!empty($row['title'])): ?>
                                    <h4 class="lex-vertical-steps__title"><?php echo $row['title']; ?></h4>
                                <?php endif ?>

                                <?php if (!empty($row['description'])): ?>
                                    <p class="lex-vertical-steps__desc"><?php echo $row['description']; ?></p>
                                <?php endif ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="lex-vertical-steps__list lex-vertical-steps__list-mobile swiper vertical-steps-slider">
                        <div class="swiper-wrapper">
                            <?php foreach ($repeater as $row): ?>
                                <div class="lex-vertical-steps__list-item swiper-slide">
                                    <div class="number"><span><?php echo $row['number']; ?></span></div>
<<<<<<< Updated upstream

=======
>>>>>>> Stashed changes
                                    <?php if (!empty($row['title'])): ?>
                                        <h4 class="lex-vertical-steps__title"><?php echo $row['title']; ?></h4>
                                    <?php endif ?>

                                    <?php if (!empty($row['description'])): ?>
                                        <p class="lex-vertical-steps__desc"><?php echo $row['description']; ?></p>
                                    <?php endif ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="vertical-steps-slider-pagination"></div>
                    </div>
                <?php endif; ?>

                <?php if (!empty($primary_button)) :
                    $link_target = $primary_button['target'] ? $primary_button['target'] : '_self'; ?>
                    <div class="lex-vertical-steps__button">
                        <a class="lex-btn lex-btn_icon lex-btn_primary"
                           href="<?php echo esc_url($primary_button['url']); ?>" target="<?php echo $link_target; ?>">
                            <?php echo esc_html($primary_button['title']); ?>
                            <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
