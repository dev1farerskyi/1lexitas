import "./app/gutenberg";
import Swiper from "swiper/swiper-bundle";
import { gsap } from "./app/gsap/gsap";
import { ScrollTrigger } from "./app/gsap/ScrollTrigger";
import { isEven, isjQuery, Coordinates, videoResize } from "./app/functions";

gsap.registerPlugin(ScrollTrigger);

(function ($) {
    $(document).ready(function () {
        // Vertical steps slider
        new Swiper(".vertical-steps-slider", {
            slidesPerView: 1.3,
            spaceBetween: 40,
            slidesOffsetBefore: 30,
            slidesOffsetAfter: 30,
            speed: 700,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: ".vertical-steps-slider-pagination",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesOffsetBefore: 0,
                    slidesOffsetAfter: 0,
                    slidesPerView: 'auto',
                    centeredSlides: true
                }
            },
        });

        // Vertical steps animate
        const verticalSteps = $(".vertical-step-animated");

        function animateVerticalSteps() {
            const activeIndex = $(".vertical-step-animated.active").index();
            const nextIndex = activeIndex + 1;

            $(".vertical-step-animated.active").removeClass("active");

            if (nextIndex < verticalSteps.length) {
                verticalSteps.eq(nextIndex).addClass("active");
            } else {
                verticalSteps.eq(0).addClass("active");
            }
        }

        setTimeout(() => {
            animateVerticalSteps()
        }, 300)

        let steps_interval = setInterval(animateVerticalSteps, 15000);

        $('.lex-vertical-steps__list-item').on('click', function () {
            let _this = $(this);

            // Change active class
            _this.parent().find('.lex-vertical-steps__list-item').removeClass('active');
            _this.addClass('active');

            // Reinit steps interval
            clearInterval(steps_interval);
            steps_interval = setInterval(animateVerticalSteps, 15000);
        });

        // Testimonials slider
        var testimonialSlider = new Swiper(".lex-testimonial__slider", {
            loop: true,
            slidesPerView: "auto",
            centeredSlides: true,
            speed: 700,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            breakpoints: {
                767: {
                    centeredSlides: false,
                    autoplay: false,
                }
            },
        });
        if ($(window).width() < 991) {
            var serviceSolutionsSlider = new Swiper(
                ".lex-service-solutions__blocks--tablet",
                {
                    loop: true,
                    slidesPerView: "auto",
                    centeredSlides: true,
                    spaceBetween: 30,
                }
            );
        }

        $(".select-2, .gfield_select").select2({
            minimumResultsForSearch: Infinity,
        });
    });

    // Sticky header
    let $header = $('.lex-header')

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 0) {
            $header.addClass('scroll-active');
        } else {
            $header.removeClass('scroll-active');
        }
    });

    // Mobile header
    $(".lex-header__burger").on("click", function () {
        if ($(this).hasClass("active")) {
            $(".lex-header").removeClass("active");
            $("body").removeClass("modal-open");
            $(this).removeClass("active");
        } else {
            $(".lex-header").addClass("active");
            $("body").addClass("modal-open");
            $(this).addClass("active");
        }
    });

    // Select2
    $('#forms_select').on('select2:select', function (e) {
        var formId = e.params.data.id;
        var form = $('#' + formId);
        $('.lex-contact-forms__form.active')
            .removeClass('active')
            .removeAttr("style");
        form
            .addClass('active')
            .animate({
                top: 0,
                opacity: 1
            }, 500);
    });

    var page = 1;
    var page_resources = 1;
    $(".lex-team-members__filter").on("click", function () {
        page = 1;
        $(".lex-team-members__filter.active").removeClass("active");
        $(this).addClass("active");
        var category = $(this).data("category");
        var request_data = {
            action: "lex_get_team_members",
            term: category,
        };
        var wrapper = $(".lex-team-members__cards-wrapper");
        var loadMoreBtn = $(".lex-team-members__btn");

        $.ajax({
            url: lex_object.ajax_url,
            type: "GET",
            data: request_data,
            beforeSend: function (xhr) {
                wrapper.html('<div class="lex-loader"></div>');
            },
            success: function (data) {
                var posts = data.content;
                var maxPages = data.max_pages;

                $(".lex-loader").remove();
                wrapper.attr("data-max-pages", maxPages);
                wrapper.hide().html(posts).fadeIn();

                if (maxPages < 2) {
                    loadMoreBtn.hide();
                } else {
                    loadMoreBtn.show();
                }
            },
        });

    });

    $('.team-members-mobile').on('select2:select', function (e) {
        var category = e.params.data.id;
        page = 1;
        var request_data = {
            action: "lex_get_team_members",
            term: category,
        };
        var wrapper = $(".lex-team-members__cards-wrapper");
        var loadMoreBtn = $(".lex-team-members__btn");
        console.log(111);
        $.ajax({
            url: lex_object.ajax_url,
            type: "GET",
            data: request_data,
            beforeSend: function (xhr) {
                wrapper.html('<div class="lex-loader"></div>');
            },
            success: function (data) {
                var posts = data.content;
                var maxPages = data.max_pages;

                $(".lex-loader").remove();
                wrapper.attr("data-max-pages", maxPages);
                wrapper.hide().html(posts).fadeIn();

                if (maxPages < 2) {
                    loadMoreBtn.hide();
                } else {
                    loadMoreBtn.show();
                }
            },
        });
    });

    $(".team-members-more").on("click", function () {
        var category = $(".lex-team-members__filter.active").data("category");
        page++;
        var request_data = {
            action: "load_more",
            term: category,
            page: page,
        };
        var wrapper = $(".lex-team-members__cards-wrapper");
        var loadMoreBtn = $(".lex-team-members__btn");

        if (wrapper.attr("data-max-pages") <= page) {
            loadMoreBtn.hide();
        }

        $.ajax({
            url: lex_object.ajax_url,
            type: "GET",
            data: request_data,
            beforeSend: function (xhr) {
                wrapper.append('<div class="lex-loader"></div>');
            },
            success: function (data) {
                $(".lex-loader").remove();
                $(data).hide().appendTo(".lex-team-members__cards-wrapper").fadeIn();
            },
        });
    });

    $(".resources-more").on("click", function () {
        page_resources++;
        var request_data = {
            action: "load_resources_posts",
            page: page_resources,
        };
        var wrapper = $(".lex-additional-resources__wrapper");

        $.ajax({
            url: lex_object.ajax_url,
            type: "GET",
            data: request_data,
            beforeSend: function (xhr) {
                wrapper.append('<div class="lex-loader"></div>');
            },
            success: function (data) {
                wrapper.append(data);
                $(".lex-loader").remove();
                $(".resources-more").remove();
            },
        });
    });

    $('.js-magnific-video').magnificPopup({
        type: 'iframe'
    });

    $('.lex-header__menu > .menu-item-has-children').each(function(e) {
        var el = $(this);

        el.prepend('<span class="submenu-toggler"></span>')
    });

    $('.submenu-toggler').on('click', function() {
        var submenu = $(this).siblings('.sub-menu');

        if(submenu.hasClass('active')) {
            submenu.removeClass('active');
            $(this).removeClass('opened');
        } else {
            submenu.addClass('active');
            $(this).addClass('opened');
        }
    });

    let BCNoSectionElement = () => {
        let $wrapGlob = $('.main-wrapper > *')
        $wrapGlob.each(function () {
            let $self = $(this)

            if (!$self.hasClass('lex-section-element') && this.tagName !== 'STYLE') {
                $self.addClass('lex-section-default-element')
            }
        });
    }

    if (window.acf) {
        window.acf.addAction('render_block_preview', BCNoSectionElement)
    } else {
        BCNoSectionElement()
    }


})(jQuery);


