<?php
/*
 * Block Name: Prefooter component Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$description = get_field('description');
$text = get_field('text');
$gradient_size = get_field('gradient_size');
$primary_button = get_field('primary_button');
$secondary_button = get_field('secondary_button');

$icon = get_field('icon');

$block_name = 'lex-prefooter-component';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name, $gradient_size );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="lex-prefooter-component__wrap">
                    <?php if ( ! empty( $icon ) ) : ?>
                        <img class="lex-prefooter-component__icon mb-30" src="<?php echo $icon['url']; ?>" alt="">
                    <?php endif; ?>
                    <?php if ( ! empty( $title ) ) : ?>
                        <h2 class="lex-prefooter-component__title" data-aos="fade-up" data-aos-duration="1000"><?php echo $title; ?></h2>
                    <?php endif; ?>
                    <?php if ( ! empty( $description ) ) : ?>
                        <p class="lex-prefooter-component__description"><?php echo $description; ?></p>
                    <?php endif; ?>
                    <div class="lex-prefooter-component__wrap-bottom">
                        <?php if ( ! empty( $text ) ) : ?>
                            <p class="lex-prefooter-component__text"><?php echo $text; ?></p>
                        <?php endif; ?>
                        <?php if ( ! empty( $primary_button ) ) :
                            $link_target = $primary_button['target'] ? $primary_button['target'] : '_self'; ?>
                            <a class="lex-btn lex-btn lex-btn_icon lex-btn_primary" href="<?php echo $primary_button['url']; ?>" target="<?php echo $link_target; ?>">
                                <?php echo $primary_button['title']; ?>
                                <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( ! empty( $secondary_button ) ) :
                            $link_target = $secondary_button['target'] ? $secondary_button['target'] : '_self'; ?>
                            <a class="lex-btn lex-btn_icon lex-btn_icon-blue lex-btn_secondary" href="<?php echo $secondary_button['url']; ?>" target="<?php echo $link_target; ?>">
                                <?php echo $secondary_button['title']; ?>
                                <?php get_template_part('template-parts/elements/secondary-btn-circle'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="lex-prefooter-component__wrap-back" data-aos="fade-up" data-aos-duration="1000"></div>
                </div>
            </div>
        </div>
    </div>
</div>