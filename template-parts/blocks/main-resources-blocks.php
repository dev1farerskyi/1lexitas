<?php
/*
 * Block Name: Main resources blocks Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$display_order 	 = get_field('display_order');
$display_order = ! empty( $args['display_order'] ) ? $args['display_order'] : $display_order;

$items = get_field('items');
$items = ! empty( $args['items'] ) ? $args['items'] : $items;

$block_name = 'lex-main-resources-blocks';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';

$args = array(
    'post_type' => 'resource',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'orderby' => 'date',
    'order' => 'DESC'
);

if ($display_order === 'manual') {
    $args['post__in'] = $items;
}

$wp_query = new WP_Query($args);
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="lex-main-resources-blocks__bottom">
            <div class="row">
                <?php if ($wp_query->have_posts()) :
                    while ($wp_query->have_posts()) : $wp_query->the_post();
                        $description = get_field('description', get_the_ID() );
                        $index = $wp_query->current_post;

                        if ( !$index ) : ?>
                            <div class="col-lg-12">
                                <div class="lex-main-resources-blocks__main" data-aos="fade-up" data-aos-duration="1000">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <?php if ( has_post_thumbnail() ): ?>
                                                <div class="lex-main-resources-blocks__main-image">
                                                    <?php the_post_thumbnail(); ?>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="lex-main-resources-blocks__main-text-wrap">
                                                <div class="lex-main-resources-blocks__main-text">
                                                    <h3 class="lex-main-resources-blocks__title mb-20"><?php the_title() ?></h3>
                                                    <?php if ( ! empty( $description ) ) : ?>
                                                        <p class="lex-main-resources-blocks__description mb-40"><?php echo $description; ?></p>
                                                    <?php endif ?>
                                                    <a class="lex-btn lex-btn_icon lex-btn_primary" href="<?php the_permalink() ?>">
                                                        <?php esc_html_e('Read Full Article', V_PREFIX); ?>
                                                        <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="lex-main-resources-blocks__card" data-aos="fade-up" data-aos-duration="1200">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <?php if ( has_post_thumbnail() ): ?>
                                                <div class="lex-main-resources-blocks__card-image">
                                                    <?php the_post_thumbnail(); ?>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="lex-main-resources-blocks__card-info-wrap">
                                                <div class="lex-additional-resources__card-info lex-main-resources-blocks__card-info">
                                                    <div class="mb-20">
                                                        <h3 class="lex-additional-resources__title"><?php the_title() ?></h3>
                                                    </div>
                                                    <a class="lex-additional-resources__link" href="<?php the_permalink() ?>">
                                                        <?php esc_html_e('Read More', V_PREFIX); ?>
                                                        <img class="circle-link" src="<?php echo V_TEMP_URL . '/assets/img/circle-icon-1.svg'; ?>" alt=""/>
                                                        <svg class="hover-svg" width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <circle cx="10.5" cy="10.5" r="10" fill="#D7E406" stroke="#D7E406"/>
                                                            <circle cx="10.5" cy="10.5" r="4.75" fill="white" stroke="#D7E406"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif;
                    endwhile; wp_reset_postdata();
                endif; ?>
            </div>
        </div>
    </div>
</div>