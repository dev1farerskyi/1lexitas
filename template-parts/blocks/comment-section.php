<?php
/*
 * Block Name: Comment Section Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$image = get_field('image');
$name = get_field('name');
$position = get_field('position');
$text = get_field('text');

$block_name = 'lex-comment-section';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="lex-comment-section__wrap">
            <div class="lex-comment-section__top mb-30">
                <?php if ( ! empty( $image ) ): ?>
                    <div class="lex-comment-section__image">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="">
                    </div>
                <?php endif ?>
                <div class="lex-comment-section__info">
                    <?php if ( ! empty( $name ) ) : ?>
                        <p class="lex-comment-section__name"><?php echo $name; ?></p>
                    <?php endif ?>
                    <?php if ( ! empty( $position ) ) : ?>
                        <p class="lex-comment-section__position"><?php echo $position; ?></p>
                    <?php endif ?>
                </div>
            </div>
            <?php if ( ! empty( $text ) ) : ?>
                <p class="lex-comment-section__text"><?php echo $text; ?></p>
            <?php endif ?>
        </div>
    </div>
</div>
