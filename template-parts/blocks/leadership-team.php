<?php
/*
 * Block Name: Leadership team Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$title = get_field('title');
$items = get_field('items');

$args = array(
    'post_type' => 'team_member',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post__in' => $items,
    'orderby' => 'post__in'
);
$items_query = new WP_Query($args);

$block_name = 'lex-leadership-team';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>" data-aos="circles-animation">
    <div class="container">
        <?php if (!empty($title)) : ?>
            <h3 class="lex-leadership-team__title"><?php echo $title; ?></h3>
        <?php endif; ?>
        <div class="lex-leadership-team__cards">
            <?php
            if ($items_query->have_posts()) : ?>
                <div class="row">
                    <?php while ($items_query->have_posts()) : $items_query->the_post(); ?>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <?php get_template_part('template-parts/elements/leadership-team-card'); ?>
                        </div>
                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
