<?php
/*
 * Block Name: Text and media Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$image = get_field('image');
$title = get_field('title');
$description = get_field('description');
$button = get_field('button');

$media_type = get_field('media_type');
$media_type = ! empty( $args['media_type'] ) ? $args['media_type'] : $media_type;

$image = get_field('image');
$image = ! empty( $args['image'] ) ? $args['image'] : $image;
$cover_image = get_field('cover_image');
$video = get_field('video', false, false);

$block_name = 'lex-text-and-image-and-button';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>" data-aos="circles-animation">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?php if ( $media_type === 'image') : ?>
                    <div class="lex-text-and-image-and-button__image" data-aos="fade-up" data-aos-offset="300" data-aos-duration="700">
                        <?php if( !empty( $image ) ): ?>
                            <img class="lex-video-and-content__photo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                            <img class="lex-text-and-image-and-button__first-svg" src="<?php echo V_TEMP_URL . '/assets/img/element-decor-2.svg'; ?>" alt=""/>
                            <img class="lex-text-and-image-and-button__second-svg" src="<?php echo V_TEMP_URL . '/assets/img/element-decor-1.svg'; ?>" alt=""/>
                        <?php endif; ?>
                    </div>
                <?php elseif ( $media_type === 'video') : ?>
                    <?php if ( ! empty( $cover_image ||  $video) ) : ?>
                        <div class="lex-text-and-image-and-button__image" data-aos="fade-up" data-aos-offset="300" data-aos-duration="700">
                            <img class="lex-video-and-content__photo" src="<?php echo $cover_image['url']; ?>" alt="<?php echo $cover_image['alt']; ?>"/>
                            <a class="lex-text-and-image-and-button__third-svg js-magnific-video" href="<?php echo $video; ?>">
                                <img src="<?php echo V_TEMP_URL . '/assets/img/play.svg'; ?>" alt=""/>
                            </a>
                        </div>
                    <?php endif ?>
                <?php endif ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="lex-text-and-image-and-button__content"
                     data-aos="fade-up"
                     data-aos-offset="300"
                     data-aos-duration="700"
                     data-aos-delay="200">
                    <div class="lex-text-and-image-and-button__content-wrap">
                        <?php if (!empty($title)) : ?>
                            <h3 class="lex-text-and-image-and-button__title"><?php echo $title; ?></h3>
                        <?php endif; ?>
                        <?php if (!empty($description)) : ?>
                            <p class="lex-text-and-image-and-button__description mb-40"><?php echo $description; ?></p>
                        <?php endif; ?>
                        <?php if ( ! empty( $button ) ) :
                            $link_target = $button['target'] ? $button['target'] : '_self'; ?>
                            <a class="lex-btn lex-btn_icon lex-btn_primary" href="<?php echo $button['url']; ?>" target="<?php echo $link_target; ?>">
                                <?php echo $button['title']; ?>
                                <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
