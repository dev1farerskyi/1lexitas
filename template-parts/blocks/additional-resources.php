<?php
/*
 * Block Name: Additional Resources Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');

$block_name = 'lex-additional-resources';

$args = array(
    'post_type' => 'resource',
    'post_status' => 'publish',
    'posts_per_page' => 6,
    'orderby' => 'DESC'
);
$items_query = new WP_Query($args);

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if (!empty($title)) : ?>
            <h1 class="lex-additional-resources__main-title"><?php echo $title; ?></h1>
        <?php endif; ?>

        <?php if ($items_query->have_posts()) : ?>
            <div class="row lex-additional-resources__wrapper">
                <?php while ($items_query->have_posts()) : $items_query->the_post(); ?>
                    <?php get_template_part('template-parts/elements/single-source'); ?>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>

        <?php if ($items_query->max_num_pages >= 2): ?>
            <div class="lex-additional-resources__btns">
                <button class="lex-btn lex-btn_icon lex-btn_primary resources-more">
                    <?php esc_html_e('View All Articles', V_PREFIX); ?>
                    <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                </button>
            </div>
        <?php endif; ?>
    </div>
</div>
