<?php

/**
 * ACF Options page support
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}


/**
 * Get button
 *
 * @param $button
 * @param $classes
 * @return void
 */
function lex_get_button($button, $classes = '') {
    if ( ! empty( $button ) ) :
        $link_target = ! empty( $button['target'] ) ? $button['target'] : '_self';
        ?>
        <a class="<?php echo $classes; ?>" href="<?php echo $button['url']; ?>" target="<?php echo esc_attr( $link_target ); ?>">
            <?php echo $button['title']; ?>
        </a>
    <?php endif;
}


function lex_change_form_submit_button($button, $form)
{
    $class_name = 'lex-btn lex-btn lex-btn_icon lex-btn_primary';
    $button_text = 'Schedule Meeting';
    $gradientId = rand();
    $button_element = '<svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10.5" cy="10.4999" r="10" stroke="white"/>
<circle cx="10.5" cy="10.4999" r="4.75" fill="url(#' . $gradientId . ')" stroke="white"/>
<defs>
<linearGradient id="' . $gradientId . '" x1="11.1825" y1="20.0024" x2="11.1825" y2="0.787378" gradientUnits="userSpaceOnUse">
<stop stop-color="#81B041"/>
<stop offset="1" stop-color="#D7E406"/>
</linearGradient> 
</defs>
</svg>';
    return '<button type="submit" class="gform_button ' . $class_name . '" id="gform_submit_button_' . $form['id'] . '">' . $button_text .
        $button_element . '</button>';
}
add_filter('gform_submit_button_1', 'lex_change_form_submit_button', 10, 2);

function lex_change_form_submit_button_2($button, $form)
{
    $class_name = 'lex-btn lex-btn lex-btn_icon lex-btn_primary';
    $button_text = 'Get in touch';
    $gradientId = rand();
    $button_element = '<svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10.5" cy="10.4999" r="10" stroke="white"/>
<circle cx="10.5" cy="10.4999" r="4.75" fill="url(#' . $gradientId . ')" stroke="white"/>
<defs>
<linearGradient id="' . $gradientId . '" x1="11.1825" y1="20.0024" x2="11.1825" y2="0.787378" gradientUnits="userSpaceOnUse">
<stop stop-color="#81B041"/>
<stop offset="1" stop-color="#D7E406"/>
</linearGradient> 
</defs>
</svg>';
    return '<button type="submit" class="gform_button ' . $class_name . '" id="gform_submit_button_' . $form['id'] . '">' . $button_text .
        $button_element . '</button>';
}
add_filter('gform_submit_button_2', 'lex_change_form_submit_button_2', 10, 2);

/**
 * Courses filter
 */
function lex_get_team_members() {
    if ( ! empty( $_GET['term'] ) ) {
        $query_args = array(
            'post_type' => 'team_member',
            'post_status' => 'publish',
            'posts_per_page' => 9,
            'paged' => $_GET['page'],
            'orderby' => 'title',
            'order' => 'asc',
            'tax_query' => array(
                array(
                    'taxonomy' => 'department',
                    'field'    => 'slug',
                    'terms'    => $_GET['term'],
                ),
            ),
        );

        $items_query = new WP_Query($query_args);

        if ( $items_query->have_posts() ) {
            ob_start();

            while ($items_query->have_posts()):
                $items_query->the_post();
                get_template_part('template-parts/elements/team-members-card');
                ?>
            <?php endwhile;
            wp_reset_postdata();

            $max_pages = $items_query->max_num_pages;

            $content = ob_get_clean();

            $response = [
                'content' => $content,
                'max_pages' => $max_pages
            ];

            wp_send_json($response);
        }
    }
    die;
}
add_action( 'wp_ajax_lex_get_team_members', 'lex_get_team_members' );
add_action( 'wp_ajax_nopriv_lex_get_team_members', 'lex_get_team_members' );

function load_more() {
    if ( ! empty( $_GET['term'] ) ) {
        $query_args = array(
            'post_type' => 'team_member',
            'post_status' => 'publish',
            'posts_per_page' => 9,
            'paged' => $_GET['page'],
            'orderby' => 'title',
            'order' => 'asc',
            'tax_query' => array(
                array(
                    'taxonomy' => 'department',
                    'field'    => 'slug',
                    'terms'    => $_GET['term'],
                ),
            ),
        );

        $items_query = new WP_Query($query_args);

        if ( $items_query->have_posts() ) {

            while ($items_query->have_posts()):
                $items_query->the_post();
                get_template_part('template-parts/elements/team-members-card');
                ?>
            <?php endwhile;
            wp_reset_postdata();

        }
    }
    die;
}
add_action( 'wp_ajax_load_more', 'load_more' );
add_action( 'wp_ajax_nopriv_load_more', 'load_more' );

function load_resources_posts() {
    $query_args = array(
        'post_type' => 'resource',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'paged' => $_GET['page'],
        'orderby' => 'title',
    );

    $items_query = new WP_Query($query_args);

    if ( $items_query->have_posts() ) {

        while ($items_query->have_posts()):
            $items_query->the_post();
            get_template_part('template-parts/elements/single-source');
            ?>
        <?php endwhile;
        wp_reset_postdata();

    }

    die;
}
add_action( 'wp_ajax_load_resources_posts', 'load_resources_posts' );
add_action( 'wp_ajax_nopriv_load_resources_posts', 'load_resources_posts' );