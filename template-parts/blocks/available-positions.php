<?php
/*
 * Block Name: Available Positions Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$content = get_field('content');
$title = get_field('title');

$block_name = 'lex-available-positions';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div data-aos="fade-up-right" data-aos-duration="1000">
            <?php if ( ! empty( $title ) ): ?>
                <h1 class="lex-available-positions__title"><?php echo $title; ?></h1>
            <?php endif ?>
        </div>
        <?php if ( ! empty( $content ) ): ?>
            <div>
                <?php echo $content; ?>
            </div>
        <?php endif ?>
    </div>
</div>