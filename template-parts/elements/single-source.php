<?php
$post_id = get_the_ID();
$linkedin = get_field('linkedin', $post_id);
?>

<div class="col-lg-4 col-md-6 col-sm-12" data-aos="fade-up" data-aos-duration="1200">
    <div class="lex-additional-resources__card">
        <div class="row">
            <?php if ( has_post_thumbnail() ): ?>
                <div class="col-lg-12">
                    <div class="lex-text-and-image__image">
                        <?php the_post_thumbnail(); ?>
                    </div>
                </div>
            <?php endif ?>

            <div class="col-lg-12">
                <div class="lex-additional-resources__card-info">
                    <?php if ( ! empty( $linkedin ) ) : ?>
                        <a href="<?php echo $linkedin['url']; ?>"><img class="lex-additional-resources__linkedin" src="<?php echo V_TEMP_URL . '/assets/img/linkedin-blue.svg'; ?>" alt=""></a>
                    <?php endif; ?>
                    <div class="mb-20">
                        <h3 class="lex-additional-resources__title"><?php the_title() ?></h3>
                    </div>
                    <a href="<?php the_permalink() ?>" class="lex-additional-resources__link">
                        <?php esc_html_e('Read More', V_PREFIX); ?>
                        <img class="circle-link" src="<?php echo V_TEMP_URL . '/assets/img/circle-icon-1.svg'; ?>" alt=""/>
                        <svg class="hover-svg" width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="10.5" cy="10.5" r="10" fill="#D7E406" stroke="#D7E406"/>
                            <circle cx="10.5" cy="10.5" r="4.75" fill="white" stroke="#D7E406"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>