<?php
/*
 * Block Name: Video and content Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$title = ! empty( $args['title'] ) ? $args['title'] : get_field('title');
$primary_button = get_field('primary_button');
if( $primary_button ):
    $primary_button_url = $primary_button['url'];
    $primary_button_title = $primary_button['title'];
endif;
$video = get_field('video', false, false);
$cover_image = get_field('cover_image');

$block_name = 'lex-video-and-content';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="lex-video-and-content__wrap">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="lex-video-and-content__content">
                        <div class="lex-video-and-content__content-wrap">
                            <?php if (!empty($title)) : ?>
                                <h3 class="lex-video-and-content__title mb-40"><?php echo $title; ?></h3>
                            <?php endif; ?>
                            <?php if (!empty($primary_button)) : ?>
                                <div class="lex-video-and-content__btn">
                                    <a class="lex-btn lex-btn_icon lex-btn_primary" href="<?php echo esc_url( $primary_button_url ); ?>">
                                        <?php echo esc_html( $primary_button_title ); ?>
                                        <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if( !empty( $video ) ): ?>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="lex-video-and-content__image">
                            <div class="lex-main-page-header__image-border">
                                <img src="<?php echo V_TEMP_URL . '/assets/img/dashed-circle.svg'; ?>" alt=""/>
                                <div class="lex-main-page-header__image-border-circle"></div>
                            </div>
                            <?php if ( ! empty( $cover_image ) ) : ?>
                                <div class="lex-video-and-content__image-wrapper">
                                    <img class="lex-video-and-content__photo" src="<?php echo $cover_image['url']; ?>" alt="<?php echo $cover_image['alt']; ?>"/>
                                    <a class="lex-text-and-image-and-button__third-svg js-magnific-video" href="<?php echo $video; ?>">
                                        <img src="<?php echo V_TEMP_URL . '/assets/img/play.svg'; ?>" alt=""/>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
