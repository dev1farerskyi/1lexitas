<?php
/*
 * Block Name: Contact Forms Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$show_custom_form = get_field('show_custom_form');
$form = get_field('form');
$custom_form = get_field('custom_form');
$image = get_field('image');
$address = get_field('address');
$phone = get_field('phone');
$description = get_field('description');

$block_name = 'lex-contact-forms';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="lex-contact-forms__wrap">
<!--            --><?php
//            ?>
<!--             <div class="lex-contact-forms__search">-->
<!--                   <div class="lex-contact-forms__search-wrap">-->
<!--                      --><?php //if (!empty($title)) : ?>
<!--                            <div class="lex-contact-forms__search-title">--><?php //echo $title; ?><!--</div>*/-->
<!--                      --><?php //endif ?>
<!--                       <div class="lex-contact-forms__search-select">-->
<!--                           <select class="select-2" id="forms_select">-->
<!--                                --><?php //$i = 1;
//                              foreach ($content as $index => $row):
//                                    if (!empty($row['dropdown_text'])) : ?>
<!--                                        <option class="lex-contact-forms__search-select-option"-->
<!--                                               value="--><?php //echo 'option-' . $index; ?><!--"*/-->
<!--                                           --><?php //selected($i, 1); ?><!-->*/-->
<!--                                           --><?php //echo $row['dropdown_text']; ?>
<!--                                       </option>-->
<!--                                    --><?php //endif;
//                                    $i++;
//                               endforeach; ?>
<!--                            </select>-->
<!--                       </div>-->
<!--                   </div>-->
<!--               </div>-->
<!--               --><?php
//            ?>

            <div class="lex-contact-forms__form">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="lex-contact-forms__form-main">
                            <div class="lex-contact-forms__form-main-count">
                                <?php if (!empty($show_custom_form)):
                                    echo $custom_form;
                                else:
                                    if (!empty($form)):
                                        echo do_shortcode('[gravityform id="' . $form['id'] . '" ajax="true" title="false" description="false"]');
                                    endif;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="lex-contact-forms__form-info">
                            <?php if (!empty($image)): ?>
                                <div class="lex-contact-forms__form-info-image">
                                    <img src="<?php echo esc_url($image['url']); ?>" alt=""/>
                                </div>
                            <?php endif ?>
                            <div class="lex-contact-forms__form-info-wrap">
                                <?php if (!empty($description)) : ?>
                                    <h4 class="lex-contact-forms__form-info-title mb-20">
                                        <?php echo $description; ?>
                                    </h4>
                                <?php endif ?>
                                <div class="lex-contact-forms__form-info-text">
                                    <?php if (!empty($address)) : ?>
                                        <div class="lex-contact-forms__form-info-address">
                                            <?php echo $address; ?>
                                        </div>
                                    <?php endif ?>
                                    <?php if (!empty($phone)) : ?>
                                        <a class="lex-contact-forms__form-info-tell"
                                           href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                                    <?php endif ?>
                                </div>
                                <div class="lex-contact-forms__form-info-wrap-wrap">
                                    <?php if (!empty($description)) : ?>
                                        <h4 class="lex-contact-forms__form-info-title mb-20">
                                            <?php echo $description; ?>
                                        </h4>
                                    <?php endif ?>
                                    <div class="lex-contact-forms__form-info-text">
                                        <?php if (!empty($address)) : ?>
                                            <div class="lex-contact-forms__form-info-address">
                                                <?php echo $address; ?>
                                            </div>
                                        <?php endif ?>
                                        <?php if (!empty($phone)) : ?>
                                            <a class="lex-contact-forms__form-info-tell" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
