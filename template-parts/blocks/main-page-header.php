<?php
/*
 * Block Name: Main Page Header Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$description = get_field('description');
$primary_button = get_field('primary_button');
$secondary_button = get_field('secondary_button');


$image = get_field('image');
$name = get_field('name');
$position = get_field('position');
$add_elements = get_field('add_elements');
$bg_style = get_field('bg_style');

$block_name = 'lex-main-page-header';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = $bg_style;
$wrapper = $add_elements ? 'add_elements' : '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="bg-circle"></div>
    <div class="lex-main-page-header__inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-col">
                    <div class="lex-main-page-header__content">
                        <?php if (!empty($title)) : ?>
                            <h2 class="lex-main-page-header__title mb-20" data-aos="fade-up" data-aos-duration="1000">
                                <?php echo $title; ?>
                            </h2>
                        <?php endif ?>
                        <?php if (!empty($description)): ?>
                            <p class="lex-main-page-header__description mb-40" data-aos="fade-up"
                               data-aos-duration="1000"><?php echo $description; ?></p>
                        <?php endif ?>
                        <div class="lex-main-page-header__btns">
                            <?php if (!empty($primary_button)) :
                                $link_target = $primary_button['target'] ? $primary_button['target'] : '_self'; ?>
                                <a class="lex-btn lex-btn_icon lex-btn_primary"
                                   data-aos="fade-up"
                                   data-aos-delay="300"
                                   data-aos-duration="800"
                                   href="<?php echo $primary_button['url']; ?>" target="<?php echo $link_target; ?>">
                                    <?php echo $primary_button['title']; ?>
                                    <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                                </a>
                            <?php endif; ?>
                            <?php if (!empty($secondary_button)) :
                                $link_target = $secondary_button['target'] ? $secondary_button['target'] : '_self'; ?>
                                <a class="lex-btn lex-btn_icon lex-btn_icon-blue lex-btn_secondary"
                                   data-aos="fade-up"
                                   data-aos-delay="600"
                                   data-aos-duration="800"
                                   href="<?php echo $secondary_button['url']; ?>" target="<?php echo $link_target; ?>">
                                    <?php echo $secondary_button['title']; ?>
                                    <?php get_template_part('template-parts/elements/secondary-btn-circle'); ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if (!empty($image)) : ?>
                    <div class="col-lg-6">
                        <div class="lex-main-page-header__image">
                            <div class="lex-main-page-header__image-border <?php echo $wrapper; ?>">
                                <img src="<?php echo V_TEMP_URL . '/assets/img/dashed-circle.svg'; ?>" alt=""/>
                            </div>
                            <div class="lex-main-page-header__image-wrapper <?php echo $wrapper; ?>">
                                <div class="circles-container">
                                    <img src="<?php echo V_TEMP_URL . '/assets/img/dotted-circle.svg'; ?>" alt=""/>
                                </div>
                                <img class="lex-main-page-header__photo" src="<?php echo $image['url']; ?>" alt=""/>
                            </div>
                        </div>
                        <?php if (!empty($name || $position)) : ?>
                            <div class="lex-main-page-header__caption">
                                <p class="lex-main-page-header__caption-name"><?php echo $name; ?></p>
                                <p class="lex-main-page-header__caption-position"><?php echo $position; ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
