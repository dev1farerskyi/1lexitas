<?php
/*
 * Block Name: Text + Image Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$main_title = get_field('main_title');
$repeater = get_field('repeater');

$block_name = 'lex-text-and-image';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>" data-aos="circles-animation">
    <div class="container container--small">
        <?php if (!empty($main_title)) : ?>
            <h2 class="lex-text-and-image__main-title"><?php echo $main_title; ?></h2>
        <?php endif; ?>
    </div>
    <?php if ( ! empty( $repeater ) ) : ?>
        <div class="container container--small">
            <?php foreach ($repeater as $row): ?>
                <div class="lex-text-and-image__card">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 lex-text-and-image__card-left" data-aos="fade-in" data-aos-duration="1200">
                            <div class="lex-text-and-image__image">
                                <div class="lex-text-and-image__image-border">
                                    <img src="<?php echo V_TEMP_URL . '/assets/img/dashed-circle.svg'; ?>" alt=""/>
                                </div>
                                <?php if (!empty($row['image'])): ?>
                                    <div class="lex-text-and-image__image-wrapper">
                                        <img class="lex-text-and-image__photo" src="<?php echo esc_url($row['image']['url']); ?>" alt=""/>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 lex-text-and-image__card-right" data-aos="fade-up" data-aos-duration="1000">
                            <div class="lex-text-and-image__card-text">
                                <?php if (!empty($row['title'])) : ?>
                                    <h3 class="lex-text-and-image__title mb-12"><?php echo $row['title']; ?></h3>
                                <?php endif ?>
                                <?php if (!empty($row['subtitle'])) : ?>
                                    <p class="lex-text-and-image__subtitle"><?php echo $row['subtitle']; ?></p>
                                <?php endif ?>
                                <?php if (!empty($row['description'])) : ?>
                                    <p class="lex-text-and-image__description">
                                        <?php echo $row['description']; ?>
                                    </p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>