<?php
/*
 * Block Name: Secondary Page Header Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = ! empty( $args['title'] ) ? $args['title'] : get_field('title');
$description = ! empty( $args['description'] ) ? $args['description'] : get_field('description');
$padding = ! empty( $args['padding'] ) ? $args['padding'] : get_field('padding');

$block_name = 'lex-secondary-page-header';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = $padding;
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>" data-aos="circles-animation">
    <div class="container-wrap">
        <div class="container">
        <div class="circle-right">
            <div class="circle-right__inner"></div>
        </div>
        <div class="circle-left">
            <div class="circle-left__inner"></div>
        </div>
        <div class="lex-secondary-page-header__content">
            <?php if (!empty($title)) : ?>
                <h1 class="lex-secondary-page-header__title mb-20"><?php echo $title; ?></h1>
            <?php endif; ?>
            <?php if (!empty($description)) : ?>
                <p class="lex-secondary-page-header__description"><?php echo $description; ?></p>
            <?php endif; ?>
        </div>
    </div>
    </div>
</div>