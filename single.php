<?php
/**
 * Single post template
 */

get_header();

if (have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div class="lex-secondary-page-header lex-section-element" data-aos="circles-animation">
            <div class="container-wrap">
                <div class="container">
                    <div class="circle-right">
                        <div class="circle-right__inner"></div>
                    </div>
                    <div class="circle-left">
                        <div class="circle-left__inner"></div>
                    </div>
                    <div class="lex-secondary-page-header__content">
                        <?php the_title('<h1 class="lex-secondary-page-header__title mb-20">', '</h1>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php the_content();
    endwhile;
endif;

get_footer();
