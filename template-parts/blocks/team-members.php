<?php
/*
 * Block Name: Team members Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$title = get_field('title');
$terms = get_terms('department');

$args = array(
    'post_type'      => 'team_member',
    'order'          => 'ASC',
    'orderby'        => 'title',
    'paged' => 1,
    'posts_per_page' => 9,
);

if ( ! empty( $terms ) ) {
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'department',
            'field'    => 'slug',
            'terms'    => $terms[0]->slug,
        ),
    );
}

$items_query = new WP_Query($args);
$pageCount = $items_query->max_num_pages;

$block_name = 'lex-team-members';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}


// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if (!empty($title)) : ?>
            <h3 class="lex-team-members__title"><?php echo $title; ?></h3>
        <?php endif ?>
        <div class="lex-team-members__filters lex-team-members__filters--desktop">
            <?php if ( ! empty($terms) && ! is_wp_error( $terms) ) {
                foreach ( $terms as $index => $term ): ?>
                    <li class="lex-team-members__filter lex-btn lex-btn_secondary <?php echo $index === 0 ? 'active' : ''; ?>"
                        data-category="<?php echo $term->slug; ?>">
                        <?php echo $term->name; ?>
                    </li>
                <?php endforeach;
                echo '</li>';
            } ?>
        </div>
        <div class="lex-team-members__filters--mobile">
            <select class="select-2 lex-team-members__filter team-members-mobile">
                <?php if ( ! empty($terms) && ! is_wp_error( $terms) ) {
                foreach ( $terms as $index => $term ): ?>
                    <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
                <?php endforeach;
                } ?>
            </select>
        </div>
        <div class="lex-team-members__cards">
            <?php
            if ($items_query->have_posts()) : ?>
                <div class="row lex-team-members__cards-wrapper" data-max-pages="<?php echo $items_query->max_num_pages; ?>"
                     data-aos="fade-up" data-aos-duration="1000">
                    <?php while ($items_query->have_posts()) : $items_query->the_post(); ?>
                        <?php get_template_part('template-parts/elements/team-members-card'); ?>
                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="lex-team-members__btn" style="<?php echo $pageCount>1 ? '' : 'display:none'; ?>">
            <button class="lex-btn lex-btn_icon lex-btn_primary team-members-more">
                <?php esc_html_e('Load More', V_PREFIX); ?>
                <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
            </button>
        </div>
    </div>
</div>