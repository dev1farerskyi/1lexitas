<?php

/**
 * Enqueue scripts.
 */
function lex_enqueue_scripts() {
    if ( is_admin() ) return false;

    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Figtree:wght@400;600;700&family=Open+Sans:wght@400;700&family=Poppins:wght@400;700&display=swap' );
    wp_enqueue_style( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/css/magnific-popup.css' );
    wp_enqueue_style( 'select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' );
    wp_enqueue_style( 'lex-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );

    wp_enqueue_script( 'select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'aos', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'lex-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true );

    wp_localize_script( 'lex-script', 'lex_object', array(
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'site_url' => site_url('/')
    ) );
}
add_action( 'wp_enqueue_scripts', 'lex_enqueue_scripts' );
