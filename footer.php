<?php
/**
 * Footer template.
 *
 * @package lex
 * @since 1.0.0
 *
 */
$linkedin = get_field('linkedin', 'option');

?>
    </div>
    <footer class="lex-footer">
        <div class="container">
            <div class="lex-footer__wrap">
                <div class="d-flex lex-footer__top">
                    <div>
                        <a href="<?php echo site_url('/'); ?>" class="lex-footer__logo" title="logo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.svg" alt="logo">
                        </a>
                    </div>
                    <div class="lex-footer__two-menus">
                        <div>
                            <?php wp_nav_menu(
                                array(
                                    'container'      => '',
                                    'items_wrap'     => '<ul class="lex-footer__menu first-footer-menu menu">%3$s</ul>',
                                    'theme_location' => 'footer-menu',
                                    'depth'          => 1,
                                    'fallback_cb'    => '__return_empty_string',
                                )
                            ); ?>
                        </div>
                        <div class="d-flex lex-footer__bottom mobile">
                            <?php wp_nav_menu(
                                array(
                                    'container'      => '',
                                    'items_wrap'     => '<ul class="lex-footer__menu second-footer-menu menu">%3$s</ul>',
                                    'theme_location' => 'copyright-menu',
                                    'depth'          => 1,
                                    'fallback_cb'    => '__return_empty_string',
                                )
                            ); ?>
                        </div>
                    </div>


                    <?php if ( ! empty( $linkedin ) ): ?>
                        <div class="d-flex lex-footer__btns mobile">
                            <a class="lex-footer__subscribe-btn" href="<?php echo( get_field('linkedin', 'option') ); ?>" target="_blank" rel="nofollow">Subscribe on
                                <span>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/linkedin.svg" alt="">
                                </span>
                            </a>
                        </div>
                    <?php endif ?>
                </div>
                <div class="d-flex lex-footer__bottom">
                    <?php wp_nav_menu(
                        array(
                            'container'      => '',
                            'items_wrap'     => '<ul class="lex-footer__menu second-footer-menu menu mobile">%3$s</ul>',
                            'theme_location' => 'copyright-menu',
                            'depth'          => 1,
                            'fallback_cb'    => '__return_empty_string',
                        )
                    ); ?>

                    <?php if ( ! empty( $linkedin ) ): ?>
                        <div class="lex-footer__btns mobile-bottom">
                            <a class="lex-footer__subscribe-btn"  href="<?php echo( get_field('linkedin', 'option') ); ?>" target="_blank" rel="nofollow">Subscribe on
                                <span>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/linkedin.svg" alt="">
                                </span>
                            </a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
	</body>
</html>
