<?php
/*
 * Block Name: Horizontal text banner Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$subtitle = get_field('subtitle');
$title = get_field('title');
$description = get_field('description');
$padding = get_field('padding');
$add_elements  = get_field('add_elements');

$button = get_field('button');

$block_name = 'lex-horizontal-text-banner';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
$wrapper = $add_elements ? 'add_elements' : '';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>" data-aos="circles-animation">
    <div class="container container--small <?php echo $wrapper; ?>">
        <div class="circle-right">
            <div class="circle-right__inner"></div>
        </div>
        <div class="circle-left">
            <div class="circle-left__inner"></div>
        </div>
        <div class="lex-horizontal-text-banner__wrap <?php echo $padding; ?>">
            <div class="lex-horizontal-text-banner__content" data-aos="fade-up" data-aos-duration="1000">
                <?php if (!empty($subtitle)) : ?>
                    <h2 class="lex-horizontal-text-banner__subtitle mb-20"><?php echo $subtitle; ?></h2>
                <?php endif; ?>
                <?php if (!empty($title)) : ?>
                    <h1 class="lex-horizontal-text-banner__title"><?php echo $title; ?></h1>
                <?php endif; ?>
                <?php if (!empty($description)) : ?>
                    <p class="lex-horizontal-text-banner__description"><?php echo $description; ?></p>
                <?php endif; ?>
                <?php if ( ! empty( $button ) ) :
                    $link_target = $button['target'] ? $button['target'] : '_self'; ?>
                    <a class="lex-btn lex-btn_icon lex-btn_icon-blue lex-btn_secondary" href="<?php echo $button['url']; ?>" target="<?php echo $link_target; ?>">
                        <?php echo $button['title']; ?>
                        <?php get_template_part('template-parts/elements/secondary-btn-circle'); ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>