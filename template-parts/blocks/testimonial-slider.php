<?php
/*
 * Block Name: Testimonial Slider Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$testimonials = get_field('testimonials');

$block_name = 'lex-testimonial';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

$args = array(
    'post_type' => 'testimonials',
    'post_status' => 'publish',
    'post_per_page' => -1,
    'post__in' => $testimonials,
    'orderby' => 'post__in'
);
$items_query = new WP_Query($args);

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>
<?php if ($items_query->have_posts()) : ?>
    <div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <img class="lex-testimonial__quote" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/left-quote.svg" alt=""/>
                    <?php if ( ! empty( $title ) ): ?>
                        <h3 class="lex-testimonial__title"><?php echo $title; ?></h3>
                    <?php endif ?>
                    <div class="swiper-buttons">
                        <div class="swiper-button-prev">
                            <img src="<?php echo V_TEMP_URL . '/assets/img/button-prev.svg'; ?>" alt=""/>
                        </div>
                        <div class="swiper-button-next">
                            <img src="<?php echo V_TEMP_URL . '/assets/img/button-next.svg'; ?>" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lex-testimonial__slider swiper" data-aos="fade-up" data-aos-offset="300" data-aos-duration="700">
                <div class="lex-testimonial__slider-image">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/testimonials-bg.jpg" alt="" />
                </div>
                <div class="swiper-wrapper">
                    <?php for($i = 1; $i <= 3; $i++) :
                    while ($items_query->have_posts()) : $items_query->the_post();
                        $post_id = get_the_ID();
                        $name = get_field( 'name', $post_id );
                        $positionplace_of_work = get_field( 'positionplace_of_work', $post_id );
                        $quote = get_field( 'quote', $post_id );
                        $image = get_field( 'image', $post_id );
                        ?>
                        <div class="lex-testimonial-block swiper-slide">
                            <div class="lex-testimonial-block__inner lex-card">
                                <div class="lex-testimonial-block__top">
                                    <?php if ( ! empty( $image ) ): ?>
                                        <img class="lex-testimonial-block__image" src="<?php echo esc_url($image['url']); ?>" alt=""/>
                                    <?php endif ?>
                                    <div class="lex-testimonial-block__caption">
                                        <?php if ( ! empty( $name ) ) : ?>
                                            <h6 class="lex-testimonial-block__name"><?php echo $name; ?></h6>
                                        <?php endif ?>
                                        <?php if ( ! empty( $positionplace_of_work ) ) : ?>
                                            <p class="lex-testimonial-block__position">
                                                <?php echo $positionplace_of_work; ?>
                                            </p>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php if ( ! empty( $quote ) ) : ?>
                                    <div class="lex-testimonial-block__description">
                                        <?php echo $quote; ?>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endwhile; endfor; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
