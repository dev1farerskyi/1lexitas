<?php
/**
 * Header template.
 *
 * @package lex
 * @since 1.0.0
 *
 */
$contact_button = get_field('contact_button', 'option');
if( $contact_button ):
    $contact_button_url = $contact_button['url'];
    $contact_button_title = $contact_button['title'];
endif;

$meeting_button = get_field('meeting_button', 'option');
if( $meeting_button ):
    $meeting_button_url = $meeting_button['url'];
    $meeting_button_title = $meeting_button['title'];
endif;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta name="format-detection" content="telephone=no" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Figtree:wght@400;600&family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<?php
// acf/main-page-header-block
$first_block_name = '';
$content = get_the_content();
if (has_blocks($content)) {
    $blocks = parse_blocks($content);
    foreach ($blocks as $block) {
        if (!empty($block['blockName'])) {
            if (!empty($block['blockName'])) $first_block_name = $block['blockName'];
            break;
        }
    }
}

$mainWrapperClass = ['main-wrapper'];
if($first_block_name !== 'acf/main-page-header-block') $mainWrapperClass[] = 'main-wrapper_padding-top';
?>

<body <?php body_class(); ?>>

    <header class="lex-header">
            <div class="container">
                <div class="lex-header__wrap">
                    <a href="<?php echo site_url('/'); ?>" class="lex-header__logo">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.svg" alt="logo">
                    </a>
                    <button class="lex-header__burger">
                        <div class="lex-header__burger-wrap">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <div class="lex-header__info">
                        <?php wp_nav_menu(
                            array(
                                'container'      => '',
                                'items_wrap'     => '<ul class="lex-header__menu menu">%3$s</ul>',
                                'theme_location' => 'main-menu',
                                'depth'          => 3,
                                'fallback_cb'    => '__return_empty_string',
                            )
                        ); ?>
                        <div class="lex-header__btns">
                            <?php if ( ! empty( $contact_button ) ): ?>
                                <a class="lex-btn lex-btn_icon lex-btn_icon-blue lex-btn_secondary" href="<?php echo esc_url( $contact_button_url ); ?>">
                                    <?php echo esc_html( $contact_button_title ); ?>
                                    <?php get_template_part('template-parts/elements/secondary-btn-circle'); ?>
                                </a>
                            <?php endif ?>
                            <?php if ( ! empty( $meeting_button ) ): ?>
                                <a class="lex-btn lex-btn_icon lex-btn_icon-blue lex-btn_primary" href="<?php echo esc_url( $meeting_button_url ); ?>">
                                    <?php echo esc_html( $meeting_button_title ); ?>
                                    <?php get_template_part('template-parts/elements/primary-btn-circle'); ?>
                                </a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>

    <div class="<?php echo esc_attr(trim(implode(' ', $mainWrapperClass))) ?>">
