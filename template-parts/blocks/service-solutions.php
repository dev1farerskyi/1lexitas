<?php
/*
 * Block Name: Service Solutions Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$title = get_field('title');
$description = get_field('description');
$circle = get_field('circle');
$blocks = get_field('blocks');

$block_name = 'lex-service-solutions';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = '';
$className[] = 'lex-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="bg-circle"></div>
    <div class="container">
        <div class="row lex-service-solutions__top">
            <div class="col-lg-6">
                <?php if ( ! empty( $title ) ) : ?>
                    <h2 class="lex-service-solutions__title mb-30"><?php echo $title; ?></h2>
                <?php endif ?>
                <?php if ( ! empty( $description ) ) : ?>
                    <p class="lex-service-solutions__description"><?php echo $description; ?></p>
                <?php endif ?>
            </div>
            <div class="col-lg-6">
                <div class="lex-service-solutions__right-col">
                    <?php if ( ! empty( $circle ) ) : ?>
                        <div class="lex-service-solutions__circle">
                            <div class="lex-service-solutions__circle-bg"></div>
                            <div class="lex-service-solutions__circle-inner">
                                <?php foreach ($circle as $row): ?>
                                    <div class="circle-segment">
                                        <?php if (!empty($row['icon'])): ?>
                                            <img class="circle-segment__icon mb-12" src="<?php echo esc_url($row['icon']['url']); ?>" alt=""/>
                                        <?php endif ?>
                                        <?php if (!empty($row['title'])) : ?>
                                            <p class="circle-segment__title"><?php echo $row['title']; ?></p>
                                        <?php endif ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if ( ! empty( $blocks ) ) : ?>
            <div class="lex-service-solutions__blocks lex-service-solutions__blocks--desktop">
                <div class="row">
                    <?php foreach ($blocks as $row): ?>
                        <div class="col-lg-4 col-md-4 lex-service-solutions__block-wrapper" data-aos="fade-up" data-aos-duration="1000">
                            <div class="lex-service-solutions__block">
                                <div class="lex-service-solutions__block-front-side">
                                    <?php if (!empty($row['icon'])): ?>
                                        <img class="lex-service-solutions__block-icon mb-12" src="<?php echo esc_url($row['icon']['url']); ?>" alt=""/>
                                    <?php endif ?>
                                    <?php if (!empty($row['title'])): ?>
                                        <p class="lex-service-solutions__block-title mb-12">
                                            <?php echo $row['title']; ?>
                                        </p>
                                    <?php endif ?>
                                    <?php if (!empty($row['description'])): ?>
                                        <p class="lex-service-solutions__block-description">
                                            <?php echo $row['description']; ?>
                                        </p>
                                    <?php endif ?>
                                </div>
                                <div class="lex-service-solutions__block-other-side">
                                    <?php if (!empty($row['image'])): ?>
                                        <img class="lex-service-solutions__block-image" src="<?php echo esc_url($row['image']['url']); ?>" alt=""/>
                                    <?php endif ?>
                                    <div class="lex-service-solutions__block-person">
                                        <?php if (!empty($row['name'])): ?>
                                            <p class="lex-service-solutions__block-name">
                                                <?php echo $row['name']; ?>
                                            </p>
                                        <?php endif ?>
                                        <?php if (!empty($row['position'])): ?>
                                            <p class="lex-service-solutions__block-position">
                                                <?php echo $row['position']; ?>
                                            </p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if ( ! empty( $blocks ) ) : ?>
            <div class="lex-service-solutions__blocks lex-service-solutions__blocks--tablet">
                <div class="swiper-wrapper">
                    <?php foreach ($blocks as $row): ?>
                        <div class="lex-service-solutions__block swiper-slide">
                            <?php if (!empty($row['icon'])): ?>
                                <img class="lex-service-solutions__block-icon mb-12" src="<?php echo esc_url($row['icon']['url']); ?>" alt=""/>
                            <?php endif ?>
                            <?php if (!empty($row['title'])): ?>
                                <p class="lex-service-solutions__block-title mb-12">
                                    <?php echo $row['title']; ?>
                                </p>
                            <?php endif ?>
                            <?php if (!empty($row['description'])): ?>
                                <p class="lex-service-solutions__block-description">
                                    <?php echo $row['description']; ?>
                                </p>
                            <?php endif ?>
                            <div class="lex-service-solutions__block-other-side">
                                <?php if (!empty($row['image'])): ?>
                                    <img class="lex-service-solutions__block-image" src="<?php echo esc_url($row['image']['url']); ?>" alt=""/>
                                <?php endif ?>
                                <div class="lex-service-solutions__block-person">
                                    <?php if (!empty($row['name'])): ?>
                                    <p class="lex-service-solutions__block-name">
                                        <?php echo $row['name']; ?>
                                    </p>
                                    <?php endif ?>
                                    <?php if (!empty($row['position'])): ?>
                                    <p class="lex-service-solutions__block-position">
                                        <?php echo $row['position']; ?>
                                    </p>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>